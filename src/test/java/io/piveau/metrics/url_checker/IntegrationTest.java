package io.piveau.metrics.url_checker;

import io.piveau.dqv.PiveauMetrics;
import io.piveau.utils.JenaUtils;
import io.piveau.vocabularies.vocabulary.PROV;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.junit5.Checkpoint;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.rxjava.ext.unit.TestContext;
import org.apache.jena.base.Sys;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(IntegrationTest.class);

    private HttpServer mockServer;


    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(UrlCheckerVerticle.class.getName(), testContext.completing());
        testContext.completeNow();
    }

    @AfterEach
    void tearDown(VertxTestContext testContext) {
        if (mockServer != null)
            mockServer.close(testContext.completing());
        testContext.completeNow();
    }

    //    @Test
    void testAccessUrl(Vertx vertx, VertxTestContext testContext) {
        testUrl(vertx, testContext, UrlType.ACCESS);
    }

    //    @Test
    void testDownloadUrl(Vertx vertx, VertxTestContext testContext) {
        testUrl(vertx, testContext, UrlType.DOWNLOAD);
    }

    /** Manually built test routine */
    @Test
    void testCsvUrl(Vertx vertx, VertxTestContext testContext) {
        checkFile("csvUrl.ttl", vertx, testContext);
    }

    @Test
    void testWMSUrl(Vertx vertx, VertxTestContext testContext) {
        checkFile("WMSUrl.ttl", vertx, testContext);
    }

    @Test
    void testWFSUrl(Vertx vertx, VertxTestContext testContext) {
        checkFile("WFSUrl.ttl", vertx, testContext);
    }

    @Test
    void testWMTSUrl(Vertx vertx, VertxTestContext testContext) {
        checkFile("WMTSUrl.ttl", vertx, testContext);
    }

    @Test
    void testWCSUrl(Vertx vertx, VertxTestContext testContext) {
        checkFile("WCSUrl.ttl", vertx, testContext);
    }

    private void checkFile(String filename, Vertx vertx, VertxTestContext testContext) {
        UrlCheckerVerticle urlVerticle = new UrlCheckerVerticle();
        urlVerticle.InitTest(vertx);
        vertx.fileSystem().readFile(filename, readResult -> {
            if (readResult.succeeded()) {
                Dataset dataset = JenaUtils.readDataset(readResult.result().getBytes(), null);
                Model defaultModel = dataset.getDefaultModel();
                List<JsonObject> requests = urlVerticle.extractUrls(defaultModel);
                requests.forEach(req -> {
                    String url = req.containsKey("accessUrl") ? req.getString("accessUrl") : req.getString("downloadUrl");
                    String method = req.getString("method");
                    Promise<JsonObject> promise = Promise.promise();
                    urlVerticle.checkUrl(url, method).onSuccess(check -> {
                        int status = check.getInteger("status");
                        req.put("status", status);
                        testContext.verify(() -> {
                            assertTrue(status >= 200 && status < 300);
                            testContext.completeNow();
                        });
                        promise.complete(req);
                    }).onFailure(promise::fail);
                });
            } else {
                testContext.failNow(readResult.cause());
            }
        });
    }

    private void testUrl(Vertx vertx, VertxTestContext testContext, UrlType urlType) {
        Checkpoint checkpoint = testContext.checkpoint(3);

        vertx.fileSystem().readFile(urlType.getLabel() + "Request.json", testContext.succeeding(urlRequest ->
                vertx.fileSystem().readFile(urlType.getLabel() + "Dqv.rdf", testContext.succeeding(dqv -> {

                    Router router = Router.router(vertx);

                    router.head("/url").handler(route ->
                            route.response().setStatusCode(200).putHeader("Content-Type", "application/json").end());

                    router.put("/callback").handler(routingContext -> {
                        if (routingContext.request().method() == HttpMethod.PUT) {
                            routingContext.request().bodyHandler(buffer -> {
                                Model dqvModel = JenaUtils.read(buffer.getBytes(), Lang.TURTLE.getContentType().getContentType());
                                Model expectedDqv = JenaUtils.read(dqv.getBytes(), Lang.TURTLE.getContentType().getContentType());

                                testContext.verify(() -> {

                                    // Dynamic timestamps cannot be included in expected RDF.
                                    // Therefore, only their existence is tested and the according properties removed.
                                    dqvModel.listStatements(null, PROV.generatedAtTime, (RDFNode) null).toList().stream()
                                            .findFirst().ifPresentOrElse(dqvModel::remove, () -> fail("No prov:generatedAtTime statement found"));

                                    log.info(JenaUtils.write(dqvModel, Lang.TURTLE));
                                    assertTrue(expectedDqv.isIsomorphicWith(dqvModel));
                                });

                                checkpoint.flag();
                            });

                            routingContext.request().response().setStatusCode(200).end(testContext.succeeding(ar -> checkpoint.flag()));
                        } else {
                            testContext.failNow(new Throwable("Unknown request"));
                        }

                        routingContext.response().setStatusCode(200).end();
                    });

                    mockServer = vertx.createHttpServer().requestHandler(router).listen(8085);

                    sendPipe(vertx, testContext, checkpoint, urlRequest.toJsonObject());
                }))));
    }

    private void sendPipe(Vertx vertx, VertxTestContext testContext, Checkpoint checkpoint, JsonObject urlCheckRequest) {
        vertx.fileSystem().readFile("pipeStub.json", testContext.succeeding(pipeStub -> {
            JsonObject pipe = new JsonObject(pipeStub);

            // inject dataset
            pipe.getJsonObject("body")
                    .getJsonArray("segments").getJsonObject(0)
                    .getJsonObject("body")
                    .getJsonObject("payload")
                    .getJsonObject("body")
                    .put("data", urlCheckRequest.toString());

            WebClient client = WebClient.create(vertx);
            client.post(8080, "localhost", "/pipe")
                    .putHeader("content-type", "application/json")
                    .expect(ResponsePredicate.SC_ACCEPTED)
                    .sendJsonObject(pipe, testContext.succeeding(response -> checkpoint.flag()));
        }));
    }

    public enum UrlType {
        ACCESS("accessUrl"), DOWNLOAD("downloadUrl");

        private String label;

        public String getLabel() {
            return label;
        }

        UrlType(String label) {
            this.label = label;
        }
    }
}
