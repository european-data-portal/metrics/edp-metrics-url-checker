package io.piveau.metrics.url_checker;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        loadConfig().onComplete(loadConfig -> {
            if (loadConfig.succeeded()) {

                JsonObject config = loadConfig.result();

                DeploymentOptions options = new DeploymentOptions()
                    .setConfig(config)
                    .setWorker(true);

                vertx.deployVerticle(UrlCheckerVerticle.class.getName(), options, deployVerticle -> {
                    if (deployVerticle.succeeded()) {
                        startPromise.complete();
                    } else {
                        startPromise.fail(deployVerticle.cause());
                        log.error("Failed to deploy UrlCheckerVerticle", deployVerticle.cause());
                    }
                });
            } else {
                startPromise.fail(loadConfig.cause());
                log.error("Failed to load config", loadConfig.cause());
            }
        });
    }

    private Future<JsonObject> loadConfig() {
        return Future.future(loadConfig -> {
            ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                    .add(ApplicationConfig.ENV_HTTP_USER_AGENT)
                    .add(ApplicationConfig.ENV_TIMEOUT_IN_MILLIS)
                    .add(ApplicationConfig.ENV_HUB_ADDRESS)
                    .add(ApplicationConfig.ENV_HUB_API_KEY)));
            ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions)).getConfig(handler -> {
                if (handler.succeeded()) {
                    log.debug(handler.result().encodePrettily());
                    loadConfig.complete(handler.result());
                } else {
                    loadConfig.fail(handler.cause());
                }
            });
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
