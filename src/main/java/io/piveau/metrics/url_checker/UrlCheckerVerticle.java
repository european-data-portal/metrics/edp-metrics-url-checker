package io.piveau.metrics.url_checker;

import io.piveau.dqv.PiveauMetrics;
import io.piveau.pipe.PipeContext;
import io.piveau.pipe.connector.PipeConnector;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.piveau.dcatap.FormatVisitor;
import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.core.*;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.*;
import org.ehcache.UserManagedCache;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.UserManagedCacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class UrlCheckerVerticle extends AbstractVerticle {

    static final String ADDRESS = "io.piveau.pipe.check.url.queue";

    private static final Logger log = LoggerFactory.getLogger(UrlCheckerVerticle.class);

    private UrlValidator validator =
            new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES + UrlValidator.ALLOW_2_SLASHES + UrlValidator.ALLOW_LOCAL_URLS);

    private WebClient webClient;

    private int timeout;

    private UserManagedCache<String, Integer> cache;

    @Override
    public void start(Promise<Void> startPromise) {

        cache = UserManagedCacheBuilder.newUserManagedCacheBuilder(String.class, Integer.class)
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(5)))
                .build(false);
        cache.init();

        timeout = config().getInteger(ApplicationConfig.ENV_TIMEOUT_IN_MILLIS, ApplicationConfig.DEFAULT_TIMEOUT_IN_MILLIS);

        WebClientOptions clientOptions = new WebClientOptions()
                .setUserAgent(config().getString(ApplicationConfig.ENV_HTTP_USER_AGENT, ApplicationConfig.DEFAULT_HTTP_USER_AGENT))
                .setConnectTimeout(10000)
                .setKeepAlive(false)
                .setMaxPoolSize(100);

        webClient = WebClient.create(vertx, clientOptions);

        vertx.eventBus().consumer(ADDRESS, this::handleIncomingPipe);

        PipeConnector.create(vertx, createPipeConnector -> {
            if (createPipeConnector.succeeded()) {
                createPipeConnector.result().consumer(UrlCheckerVerticle.ADDRESS);
                startPromise.complete();
            } else {
                startPromise.fail(createPipeConnector.cause());
                log.error("Failed to create pipe connector", createPipeConnector.cause());
            }
        });
    }

    @Override
    public void stop(Promise<Void> stopPromise) {
        webClient.close();
        cache.close();
        stopPromise.complete();
    }

    private void handleIncomingPipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        if (RDFMimeTypes.TRIG.equalsIgnoreCase(pipeContext.getMimeType())) {
            Dataset dataset = JenaUtils.readDataset(pipeContext.getStringData().getBytes(), null);
            incomingDataset(dataset, pipeContext);
        }
    }

    private void incomingDataset(Dataset dataset, PipeContext pipeContext) {
        Model defaultModel = dataset.getDefaultModel();
        List<Model> metricsModels = PiveauMetrics.listMetricsModels(dataset);
        String urn = "urn:" + JenaUtils.normalize(pipeContext.getPipe().getHeader().getContext()) + ":" + pipeContext.getPipe().getHeader().getName();
        Model metricsModel = metricsModels.isEmpty() ? PiveauMetrics.createMetricsGraph(dataset, urn) : PiveauMetrics.listMetricsModels(dataset).get(0);
        List<JsonObject> requests = extractUrls(defaultModel);
        List<Future<JsonObject>> checks = new ArrayList<>();
        requests.forEach(req -> {
            String url = req.containsKey("accessUrl") ? req.getString("accessUrl") : req.getString("downloadUrl");
            String method = req.getString("method");
            Promise<JsonObject> promise = Promise.promise();
            checks.add(promise.future());
            checkUrl(url, method).onSuccess(check -> {
                pipeContext.log().debug("URL {} check: {} - {}", url, check.getInteger("status"), check.getString("note"));
                req.put("status", check.getInteger("status"));
                promise.complete(req);
            }).onFailure(promise::fail);
        });
        CompositeFuture.join(new ArrayList<>(checks)).onComplete(ar -> {
            if (ar.succeeded()) {
                checks.forEach(f -> {
                    if (f.succeeded()) {
                        JsonObject check = f.result();
                        // if replace
                        Resource dist = metricsModel.getResource(check.getString("distributionUri"));
                        Resource urlMetric = check.containsKey("accessUrl") ? PV.accessUrlStatusCode : PV.downloadUrlStatusCode;
                        Resource url = metricsModel.createResource(check.containsKey("accessUrl") ? check.getString("accessUrl") : check.getString("downloadUrl"));

                        PiveauMetrics.removeMeasurement(metricsModel, dist, urlMetric, url);
                        PiveauMetrics.addMeasurement(metricsModel, dist, url, urlMetric, check.getInteger("status"));
                    } else {
                        pipeContext.log().error("URL check for " + pipeContext.getDataInfo().getString("uriRef", pipeContext.getDataInfo().getString("identifier")) + " failed.", ar.cause());
                    }
                });
                pipeContext.log().info("Checked " + checks.size() + " URLs: " + pipeContext.getDataInfo().encode());
                pipeContext.setResult(JenaUtils.write(dataset, RDFFormat.TRIG), RDFMimeTypes.TRIG, pipeContext.getDataInfo()).forward();
            } else {
                pipeContext.log().error("URL checks for " + pipeContext.getDataInfo().getString("uriRef", pipeContext.getDataInfo().getString("identifier")) + " failed.", ar.cause());
            }
            dataset.close();
        });
    }

    public Future<JsonObject> checkUrl(String url, String method) {
        return Future.future(checkUrl -> {
            JsonObject result = new JsonObject();
            if (url == null) {
                log.debug("Encountered invalid RDF");
                result.put("status", config().getInteger(ApplicationConfig.ENV_INVALID_RDF_STATUS, ApplicationConfig.DEFAULT_INVALID_RDF_STATUS));
                result.put("note", "URL is null");
                checkUrl.complete(result);
            } else if (!validator.isValid(url)) {
                log.debug("Encountered invalid URL: {}", url);
                result.put("status", config().getInteger(ApplicationConfig.ENV_INVALID_URL_STATUS, ApplicationConfig.DEFAULT_INVALID_URL_STATUS));
                result.put("note", "URL is invalid");
            } else {
                try {
                    String parsed = new URIBuilder(url).removeQuery().build().toString();
                    if (cache.containsKey(parsed)) {
                        result.put("status", cache.get(parsed));
                        result.put("note", "Cached value");
                        checkUrl.complete(result);
                    } else {
                        if (method.equals("HEAD")) {
                            log.debug("Check with HEAD: {}", url);
                            webClient.headAbs(url).timeout(timeout).send(head -> {
                                if (cache.containsKey(parsed)) {
                                    result.put("status", cache.get(parsed));
                                    result.put("note", "Cached value, after response");
                                    checkUrl.complete(result);
                                } else {
                                    if (head.succeeded()) {
                                        result.put("status", head.result().statusCode());
                                        result.put("note", "Response value");
                                        checkUrl.complete(result);
                                    } else {
                                        log.debug("Connection failure: {}", head.cause().getMessage());
                                        result.put("status", config().getInteger(ApplicationConfig.ENV_CONNECTION_EXCEPTION_STATUS, ApplicationConfig.DEFAULT_CONNECTION_EXCEPTION_STATUS));
                                        result.put("note", head.cause().getMessage());
                                        checkUrl.complete(result);
                                    }
                                    cache.putIfAbsent(parsed, checkUrl.future().result().getInteger("status"));
                                }
                            });
                        } else {
                            String geoUrl = getGeoUrl(url, method);
                            log.debug("Check with GET: {}", geoUrl);
                            webClient.getAbs(geoUrl).timeout(timeout).send(get -> {
                                if (cache.containsKey(parsed)) {
                                    result.put("status", cache.get(parsed));
                                    result.put("note", "Cached value, after response");
                                    checkUrl.complete(result);
                                } else {
                                    if (get.succeeded()) {
                                        result.put("status", get.result().statusCode());
                                        result.put("note", "Response value");
                                        checkUrl.complete(result);
                                    } else {
                                        log.debug("Connection failure: {}", get.cause().getMessage());
                                        result.put("status", config().getInteger(ApplicationConfig.ENV_CONNECTION_EXCEPTION_STATUS, ApplicationConfig.DEFAULT_CONNECTION_EXCEPTION_STATUS));
                                        result.put("note", get.cause().getMessage());
                                        checkUrl.complete(result);
                                    }
                                }
                                if (cache.putIfAbsent(parsed, checkUrl.future().result().getInteger("status")) == null) {
                                    log.debug("Cached {} - {}", parsed, checkUrl.future().result().getInteger("status"));
                                }
                            });
                        }
                        ;
                    }
                } catch (IllegalArgumentException | VertxException e) {
                    log.error("Checking url " + url, e);
                    result.put("status", config().getInteger(ApplicationConfig.ENV_INVALID_URL_STATUS, ApplicationConfig.DEFAULT_INVALID_URL_STATUS));
                    result.put("note", e.getMessage());
                    checkUrl.complete(result);
                } catch (Exception e) {
                    log.error("Checking url " + url, e);
                    checkUrl.fail(e);
                }
            }
        });
    }

    public List<JsonObject> extractUrls(Model model) {
        List<JsonObject> extracted = new ArrayList<>();
        model.listSubjectsWithProperty(RDF.type, DCAT.Dataset).forEachRemaining(dataset ->
                dataset.listProperties(DCAT.distribution).forEachRemaining(statement -> {
                    Resource distribution = statement.getResource();
                    JsonObject jsonObject = new JsonObject()
                            .put("datasetUri", dataset.getURI())
                            .put("distributionUri", distribution.getURI());

                    String method = "HEAD";

                    if (distribution.hasProperty(DCTerms.format)) {
                        JsonObject format = (JsonObject) distribution.listProperties(DCTerms.format).next().getObject().visitWith(FormatVisitor.INSTANCE);
                        if (format != null) {
                            switch (format.getString("id").toLowerCase()) {
                                case "wms":
                                case "wms_srvc": {
                                    method = "WMS";
                                    break;
                                }
                                case "wfs":
                                case "wfs_srvc": {
                                    method = "WFS";
                                    break;
                                }
                                case "wmts": {
                                    method = "WMTS";
                                    break;
                                }
                                case "wcs": {
                                    method = "WCS";
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                            log.debug("Extraction method for {}: {}", format.getString("id"), method);
                        }
                    }
                    jsonObject.put("method", method);

                    distribution.listProperties(DCAT.accessURL).forEachRemaining(st -> {
                        if (st.getObject().isURIResource()) {
                            extracted.add(jsonObject.copy().put("accessUrl", st.getResource().getURI()));
                        } else {
                            extracted.add(jsonObject.copy().putNull("accessUrl"));
                        }
                    });

                    distribution.listProperties(DCAT.downloadURL).forEachRemaining(st -> {
                        if (st.getObject().isURIResource()) {
                            extracted.add(jsonObject.copy().put("downloadUrl", st.getResource().getURI()));
                        } else {
                            extracted.add(jsonObject.copy().putNull("downloadUrl"));
                        }
                    });
                })
        );
        return extracted;
    }

    private String getGeoUrl(String url, String method) {
        try {
            URIBuilder builder = new URIBuilder(url);
            if (!builder.getQueryParams().contains(new BasicNameValuePair("request", "GetCapabilities"))
                    && !builder.getQueryParams().contains(new BasicNameValuePair("REQUEST", "GetCapabilities"))) {
                builder.clearParameters().setParameter("request", "GetCapabilities").setParameter("service", method);
            }
            return builder.build().toString();
        } catch (URISyntaxException e) {
            log.warn("Parsing geo URL: {}", url, e);
            return url;
        }
    }

    /**
     * Helper method for testing
     */
    public void InitTest(Vertx vertx) {
        cache = UserManagedCacheBuilder.newUserManagedCacheBuilder(String.class, Integer.class)
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(5)))
                .build(false);
        cache.init();
        timeout = 10000;
        WebClientOptions clientOptions = new WebClientOptions()
                .setConnectTimeout(10000)
                .setKeepAlive(false)
                .setMaxPoolSize(100);
        webClient = WebClient.create(vertx, clientOptions);
    }
}
