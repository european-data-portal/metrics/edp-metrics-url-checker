package io.piveau.metrics.url_checker;

final class ApplicationConfig {

    static final String ENV_INVALID_RDF_STATUS = "PIVEAU_INVALID_RDF_STATUS";
    static final Integer DEFAULT_INVALID_RDF_STATUS = 1500;

    static final String ENV_INVALID_URL_STATUS = "PIVEAU_INVALID_URL_STATUS";
    static final Integer DEFAULT_INVALID_URL_STATUS = 1300;

    static final String ENV_CONNECTION_EXCEPTION_STATUS = "PIVEAU_CONNECTION_EXCEPTION_STATUS";
    static final Integer DEFAULT_CONNECTION_EXCEPTION_STATUS = 1100;

    static final String ENV_TIMEOUT_IN_MILLIS = "PIVEAU_TIMEOUT_IN_MILLIS";
    static final Integer DEFAULT_TIMEOUT_IN_MILLIS = 10000;

    static final String ENV_HTTP_USER_AGENT = "PIVEAU_HTTP_USER_AGENT";
    static final String DEFAULT_HTTP_USER_AGENT = "Mozilla/5.0 (European Data Portal) Gecko/20100101 Firefox/40.1";

    static final String ENV_HUB_ADDRESS = "PIVEAU_HUB_ADDRESS";
    static final String ENV_HUB_API_KEY = "PIVEAU_HUB_API_KEY";

}
